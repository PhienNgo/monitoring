﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace Monitoring
{
    public class PingHealthCheck : IHealthCheck
    {
        private const string DATE_FORMAT = "HH:mm:ss fff dd/MM/yyyy";
        private string _host;
        private int _timeout;
        private int _pingInterval;
        private DateTime _lastPingTime = DateTime.MinValue;
        private HealthCheckResult _lastPingResult = HealthCheckResult.Healthy();

        public PingHealthCheck(string host, int timeout, int pingInterval = 0)
        {
            _host = host;
            _timeout = timeout;
            _pingInterval = pingInterval;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            if (_pingInterval != 0 && _lastPingTime.AddSeconds(_pingInterval) > DateTime.Now)
            {
                return _lastPingResult;
            }

            try
            {
                using (var ping = new Ping())
                {
                    _lastPingTime = DateTime.Now;

                    var reply = await ping.SendPingAsync(_host, _timeout);

                    if (reply.Status != IPStatus.Success)
                    {
                        _lastPingResult = HealthCheckResult.Unhealthy("Không thể kết nối tới server: " + _host);
                    }
                    else if (reply.RoundtripTime >= _timeout)
                    {
                        _lastPingResult = HealthCheckResult.Unhealthy($"Server {_host} chậm phản hồi trong thời gian {_timeout}");
                    }
                    else
                    {
                        _lastPingResult = HealthCheckResult.Healthy();
                    }
                }
            }
            catch
            {
                _lastPingResult = HealthCheckResult.Unhealthy("Exception: Không thể kết nối tới server: " + _host);
            }

            return _lastPingResult;
        }
    }
}
