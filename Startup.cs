﻿using System;
using System.Linq;
using HealthChecks.UI.Client;
using HealthChecks.UI.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Monitoring
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddRazorPages();
            services.AddHealthChecks().AddUrlGroup(name:"Kết nối tới DaiVietGo", uri:new Uri("http://localhost:8000/"), tags: new[] { "DaiVietGo" });
            services.AddHealthChecksUI(setupSettings: setup =>
            {
                setup.AddHealthCheckEndpoint("General Health Check", "http://localhost:55520/healthy");
                setup.AddWebhookNotification("telegram-webhook-notify",
                uri: $"https://api.telegram.org/bot{Configuration["Telegram:BotToken"]}/sendMessage?chat_id={Configuration["Telegram:GroupId"]}",
                payload: "{\"text\": \"Webhook report for [[LIVENESS]]: \r\n - [[FAILURE]] \r\n - Chi tiết: [[DESCRIPTIONS]]\"}",
                restorePayload: "{\"text\": \"[[LIVENESS]] is back to life\"}",
                customMessageFunc: (report) =>
                {
                    var failing = report.Entries.Where(e => e.Value.Status == UIHealthStatus.Unhealthy);
                    return $"{failing.Count()} healthchecks are failing";
                },
                customDescriptionFunc: report =>
                {
                    var failing = report.Entries.Where(e => e.Value.Status == UIHealthStatus.Unhealthy);
                    return $"{string.Join("/", failing.Select(f => f.Value.Description + " - " + f.Value.Tags?.FirstOrDefault()))}";
                });
                setup.SetEvaluationTimeInSeconds(60);
                setup.DisableDatabaseMigrations();
            }).AddSqliteStorage($"Data Source=sqlitehealthcheck.db");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/healthy", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapHealthChecksUI();
                endpoints.MapRazorPages();
            });
        }
    }
}
